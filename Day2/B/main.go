package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type passwordPolicy struct {
	lowest int
	highest int
	letter string
	password string
}

func main() {
	numbers := readInput()
	validPaswords := 0
	for _, polpass := range numbers {
		regex := regexp.MustCompile(`(?P<l>[0-9]*)-(?P<h>[0-9]*) (?P<letter>[a-z]): (?P<password>[a-z]*)$`)
		res := regex.FindAllStringSubmatch(polpass, -1)
		paspol := passwordPolicy{atoi(res[0][1]), atoi(res[0][2]), res[0][3], res[0][4]}
		fPos := string(paspol.password[paspol.lowest - 1]) == paspol.letter
		sPos := string(paspol.password[paspol.highest - 1]) == paspol.letter
		if (fPos || sPos) && !(fPos && sPos) {
			validPaswords++
		}
	}
	fmt.Println(validPaswords)


}

func readInput() []string {
	file, err := os.Open("Day2/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var polpass []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		i := scan.Text()
		polpass = append(polpass, i)
	}

	return polpass
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}