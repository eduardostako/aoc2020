package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	bagToFound = "shiny gold"
)

type Bag struct {
	colour string
	contains map[string]int
}

func main() {
	bags := readInput()
	result := 0
	for _, bag := range bags {
		if checkBag(bag, bags) {
			result++
		}
	}
	fmt.Println(result)
}

func checkBag(bag Bag, bags map[string]Bag) bool{
	if bag.contains == nil {
		return false
	}
	if _, ok := bag.contains[bagToFound]; ok {
		return true
	} else {
		for key := range bag.contains {
			if checkBag(bags[key], bags) {
				return true
			}
		}
	}
	return false
}

func readInput() map[string]Bag {
	file, err := os.Open("Day7/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var bags = make(map[string]Bag)

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		regex := regexp.MustCompile(`(.*) bags contain ([0-9]{1} .*  bag[s]?)*\.$`)
		res := regex.FindAllStringSubmatch(line, -1)
		//We don't care if it does not contain anything, loser.
		if res != nil {
			bag := res[0][1]
			bagsInside := strings.Split(res[0][2], ",")
			var bagsInsideMap = make(map[string]int)
			for _, bagInside := range bagsInside {
				regex := regexp.MustCompile(`([0-9]{1}) (.*) bag[s]?$`)
				res := regex.FindAllStringSubmatch(bagInside, -1)
				bagsInsideMap[res[0][2]] = atoi(res[0][1])
			}
			bags[bag] = Bag{bag, bagsInsideMap}
		}
	}
	return bags
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}