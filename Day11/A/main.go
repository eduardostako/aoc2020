package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type dir struct {
	y, x int
}

var dirs = []dir {
	{-1, -1},
	{-1, 0},
	{-1, 1},
	{0, -1},
	{0, +1},
	{1, -1},
	{1, 0},
	{1, +1},
}

func main() {

	board := readInput()
	for {
		tempBoard := generateRound(board)
		if isEqual(tempBoard, board) {
			break
		}
		board = tempBoard
	}
	occupiedSeats := 0
	for _, row := range board {
		for _, cell := range row {
			if cell == "#" {
				occupiedSeats++
			}
		}
	}
	fmt.Println(occupiedSeats)

}

func isEqual(tempBoard [][]string, board [][]string) bool{
	for i, row := range tempBoard {
		for j, cell := range row {
			if cell != board[i][j] {
				return false
			}
		}
	}
	return true
}

func generateRound(board [][]string) [][]string {
	newBoard := make([][]string, len(board))
	for i := range board {
		newBoard[i] = make([]string, len(board[i]))
		copy(newBoard[i], board[i])
	}
	for i, row := range board {
		for j, cell := range row {
			newBoard[i][j] = generateNextState(cell, board, i, j)
		}
	}
	return newBoard
}

func generateNextState(cell string, board [][]string, y int, x int) string{
	occupiedSeats := findSeats(board, y, x)
	switch cell {
	case "L":
		if occupiedSeats == 0{
			return "#"
		}
	case "#":
		if occupiedSeats >= 4 {
			return "L"
		}
	}
	return cell
}

func findSeats(board [][]string, y int, x int) int {

	occupiedSeats := 0
	for _, slope := range dirs{
		if y + slope.y >= 0 && y + slope.y < len(board) && x + slope.x >= 0 && x + slope.x < len(board[y]) && board[y + slope.y][x + slope.x] == "#"{
			occupiedSeats++
		}
	}
	return occupiedSeats
}

func readInput() [][]string {
	file, err := os.Open("Day11/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var board [][]string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		var row []string
		line := scan.Text()
		for _, cell := range line {
			row = append(row, string(cell))
		}
		board = append(board, row)

	}
	return board
}