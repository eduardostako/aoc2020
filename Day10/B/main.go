package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)


func main() {

	adapters := readInput()
	sort.Ints(adapters)
	total := getMeTheParts(adapters)
	fmt.Println(total)

}

func getMeTheParts(adapters []int) int {
	total := 1
	lastJoltage := 0
	lastCut := 0
	//cut the slice, chopchopchop
	for i := 1; i < len(adapters)-1; i++ {
		if adapters[i]-adapters[i-1] == 3 {
			total *= expandPossibilities(adapters[lastCut:i+1], lastJoltage)
			lastCut = i + 1
			lastJoltage = adapters[i]
		}
	}
	//last part of the slice, ugly
	total *= expandPossibilities(adapters[lastCut:], lastJoltage)
	return total
}

func expandPossibilities(adapters []int, currentJoltage int) int{
	count := 1
	if adapters[0] - currentJoltage > 3 {
		return 0
	}
	if len(adapters) == 1 {
		return 1
	}
	for i := 0; i < len(adapters); i++ {
		if adapters[i] - currentJoltage < 3 && i + 1 < len(adapters){
			count += expandPossibilities(adapters[i + 1:], currentJoltage)
		} else if adapters[i] - currentJoltage > 3 {
			return 0
		}
		currentJoltage = adapters[i]
	}
	return count
}

func readInput() []int {
	file, err := os.Open("Day10/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var adapters []int

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		adapters = append(adapters, atoi(line))

	}
	return adapters
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}