package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)


func main() {

	adapters := readInput()
	currentJoltage := 0
	numOneDiff := 0
	numThreeDiff := 1

	sort.Ints(adapters)

	for _, adapter := range adapters {
		switch adapter - currentJoltage {
		case 1:
			numOneDiff++
		case 2:
		case 3:
			numThreeDiff++
		default:
			panic(fmt.Sprintf("Current joltage %d is too low or to high for adapter %d", currentJoltage, adapter))
		}
		currentJoltage = adapter
	}
	fmt.Println(numOneDiff*numThreeDiff)

}

func readInput() []int {
	file, err := os.Open("Day10/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var adapters []int

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		adapters = append(adapters, atoi(line))

	}
	return adapters
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}