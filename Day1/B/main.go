package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	numbers := readInput()

	for i, number := range numbers {
		for j, second := range numbers {
			for k, third := range numbers {
				if i != j && i != k && k != j {
					if number + second + third == 2020 {
						fmt.Println(number*second*third)
					}
				}
			}
		}
	}
}

func readInput() []int {
	file, err := os.Open("Day1/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var numbers []int

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		i, err := strconv.Atoi(scan.Text())
		if err != nil {
			log.Fatal(err)
		}
		numbers = append(numbers, i)
	}

	return numbers
}