package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)



func main() {

	earliestDep := math.MaxFloat64
	var bestId int
	ids, earliest := readInput()
	for _, id := range ids {
		if id != "x" {
			timestamp := atoi(id)
			nextDep := math.Ceil(float64(earliest) / float64(timestamp)) * float64(timestamp)
			if nextDep < earliestDep {
				earliestDep = nextDep
				bestId = timestamp
			}
		}
	}

	fmt.Println((int(earliestDep) - earliest) * bestId)

}

func readInput() ([]string, int) {
	file, err := os.Open("Day13/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	scan.Scan()
	line := scan.Text()
	earliest := atoi(line)
	scan.Scan()
	line = scan.Text()
	ids := strings.Split(line, ",")


	return ids, earliest
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
