package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type span struct {
	min, max int
}

func (s span) isBetween(number int) bool{
	if number >= s.min && number <= s.max {
		return true
	}
	return false
}

func main() {
	spanMap, nearby := readInput()
	total := 0
	for _, numbers := range nearby {
		for _, number := range numbers {
			invalid := true
			for _, value := range spanMap {
				if value[0].isBetween(atoi(number)) || value[1].isBetween(atoi(number)) {
					invalid = false
					break
				}
			}
			if invalid {
				total += atoi(number)
			}
		}
	}
	fmt.Println(total)
}

func readInput() (map[string][]span, [][]string) {
	file, err := os.Open("Day16/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	spanMap := make(map[string][]span)
	var nearby [][]string
	for scan.Scan() {
		line := scan.Text()
		if strings.Contains(line, "or") {
			regex := regexp.MustCompile(`(.*): ([0-9]*-[0-9]* or [0-9]*-[0-9]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			ranges := res[0][2]
			var spans []span
			for _, limit := range strings.Split(ranges, " or ") {
				limits := strings.Split(limit, "-")
				spans = append(spans, span{atoi(limits[0]), atoi(limits[1])})
			}
			spanMap[res[0][1]] = spans
		} else if  strings.Contains(line, "nearby tickets") {

			for scan.Scan() {
				line := scan.Text()
				numbers := strings.Split(line, ",")
				nearby = append(nearby, numbers)
			}
		}
	}
	return spanMap, nearby
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}