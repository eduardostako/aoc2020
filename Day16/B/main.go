package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type span struct {
	min, max int
}

func (s span) isBetween(number int) bool{
	if number >= s.min && number <= s.max {
		return true
	}
	return false
}

func main() {
	spanMap, nearby, myTicket := readInput()
	var validTickets [][]string
	for _, numbers := range nearby {
		valid := true
		for _, number := range numbers {
			if !valid {
				break
			}
			for _, value := range spanMap {
				if !value[0].isBetween(atoi(number)) && !value[1].isBetween(atoi(number)) {
					valid = false
				} else {
					valid = true
					break
				}
			}
		}
		if valid {
			validTickets = append(validTickets, numbers)
		}
	}

	var sortedField = make(map[string]map[int]struct{})
	for key, value := range spanMap {
		var possibleField = make(map[int]struct{})
		for i, number := range validTickets[0] {
			if value[0].isBetween(atoi(number)) || value[1].isBetween(atoi(number)) {
				possibleField[i] = struct{}{}
			}
		}
		for _, numbers := range validTickets {
			for pos := range possibleField {
				if !value[0].isBetween(atoi(numbers[pos])) && !value[1].isBetween(atoi(numbers[pos])) {
					delete(possibleField, pos)
				}
			}
		}
		sortedField[key] = possibleField
	}
	sorted := false
	for !sorted {
		sorted = true
		for key, value := range sortedField {
			if len(value) == 1 {
				for pos := range value {
					for field, posMap := range sortedField {
						if field != key {
							tempMap := posMap
							delete(tempMap, pos)
							sortedField[field] = tempMap
						}
					}
				}
			}
		}
		for _, value := range sortedField {
			if len(value) != 1 {
				sorted = false
				break
			}
		}
	}
	total := 1
	for key, value := range sortedField {
		if strings.Contains(key, "departure") {
			for pos := range value {
				total *= atoi(myTicket[pos])
			}
		}
	}
 	fmt.Println(total)
}

func readInput() (map[string][]span, [][]string, []string) {
	file, err := os.Open("Day16/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	spanMap := make(map[string][]span)
	var myTicket []string
	var nearby [][]string
	for scan.Scan() {
		line := scan.Text()
		if strings.Contains(line, "or") {
			regex := regexp.MustCompile(`(.*): ([0-9]*-[0-9]* or [0-9]*-[0-9]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			ranges := res[0][2]
			var spans []span
			for _, limit := range strings.Split(ranges, " or ") {
				limits := strings.Split(limit, "-")
				spans = append(spans, span{atoi(limits[0]), atoi(limits[1])})
			}
			spanMap[res[0][1]] = spans
		} else if  strings.Contains(line, "nearby tickets") {

			for scan.Scan() {
				line := scan.Text()
				numbers := strings.Split(line, ",")
				nearby = append(nearby, numbers)
			}
		} else if  strings.Contains(line, "your ticket") {
			scan.Scan()
			line := scan.Text()
			myTicket = strings.Split(line, ",")
		}
	}
	return spanMap, nearby, myTicket
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}