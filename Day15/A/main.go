package main

import (
	"fmt"
)

type spokenNumber struct {
	lastSpoken, secondSpoken int
}

func (e *spokenNumber) isSpoken(turn int) spokenNumber {
	e.secondSpoken = e.lastSpoken
	e.lastSpoken = turn

	return *e
}

func main() {
	spoken := make(map[int]spokenNumber)
	starting := []int{6, 4, 12, 1, 20, 0, 16}
	//starting := []int{0,1,5,10,3,12,19}
	//starting := []int{0,3,6}
	//starting := []int{1,2,3}
	for i, number := range starting {
		spoken[number] = spokenNumber{i + 1, i + 1}
	}

	turn := len(starting) + 1
	lastSpoken := starting[len(starting) - 1]
	for turn < 30000001 {
		currentSpoken := spoken[lastSpoken]
		//First seen, we put a zero
		if currentSpoken.secondSpoken == currentSpoken.lastSpoken {
			spoken[0] = changeSpoken(0, spoken, turn)
			lastSpoken = 0
		} else {
			number := currentSpoken.lastSpoken - currentSpoken.secondSpoken
			spoken[number] = changeSpoken(number, spoken, turn)
			lastSpoken = number
		}
		turn++
	}
	fmt.Println(lastSpoken)
}

func changeSpoken(number int, spoken map[int]spokenNumber, turn int) spokenNumber {
	var newSpoken spokenNumber
	if value, ok := spoken[number]; ok {
		newSpoken = value.isSpoken(turn)
	} else {
		newSpoken = spokenNumber{turn, turn}
	}
	return newSpoken
}