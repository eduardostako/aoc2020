package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var passengers []string


func main() {
	readInput()
	result := 0
	for _, group := range passengers {
		totalAnswers := make(map[string]bool)
		for _, answer := range group {
			if !totalAnswers[string(answer)] {
				totalAnswers[string(answer)] = true
			}
		}
		result += len(totalAnswers)
	}
	fmt.Println(result)
}

func readInput() []string {
	file, err := os.Open("Day6/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var group string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if len(line) != 0 {
			group += line
		} else {
			passengers = append(passengers, group)
			group = ""
		}
	}
	passengers = append(passengers, group)

	return passengers
}