package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	Alphabet = "abcdefghijklmnopqrstuvwxyz"
)

var passengers [][]string


func main() {
	readInput()
	result := 0
	for _, group := range passengers {
		totalAnswers := make(map[string]bool)
		for _, letter := range Alphabet {
			totalAnswers[string(letter)] = true
		}
		for _, answers := range group {
			tempAnswers := make(map[string]bool)
			for _, answer := range answers {
				tempAnswers[string(answer)] = true
			}
			for key, value := range totalAnswers {
				if value && !tempAnswers[key] {
					totalAnswers[key] = false
				}
			}
		}
		for _, value := range totalAnswers {
			if value {
				result ++
			}
		}
	}
	fmt.Println(result)
}

func readInput() [][]string {
	file, err := os.Open("Day6/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var group []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if len(line) != 0 {
			group = append(group, line)
		} else {
			passengers = append(passengers, group)
			group = nil
		}
	}
	passengers = append(passengers, group)

	return passengers
}