package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)


func main() {

	var total int
	for _, expresion := range readInput() {
		total += calculateExpresion(expresion)
	}

	fmt.Println(total)

}

func calculateExpresion(expression string) int {
	var total int
	var operation string
	for i := 0; i < len(expression); i++ {
		if string(expression[i]) == "*" {
			operation = "*"
		} else if string(expression[i]) == "+" {
			operation = "+"
		} else if string(expression[i]) == "(" {
			found := 1
			for j, remainValue := range expression[i + 1:] {
				if string(remainValue) == ")" {
					found--
				} else if string(remainValue) == "(" {
					found++
				}
				if found == 0 {
					if len(operation) == 0 {
						total = calculateExpresion(expression[i + 1:i+j+1])
						i += j + 1
					} else {
						if operation == "*" {
							total *= calculateExpresion(expression[i + 1:i+j+1])
							operation = ""
							i += j + 1
						} else if operation == "+" {
							total += calculateExpresion(expression[i + 1:i+j+1])
							operation = ""
							i += j + 1
						}
					}
					break
				}
			}

		} else if string(expression[i]) == ")" {
			return total
		} else if isAtoi(string(expression[i])) {
			if len(operation) == 0 {
				total = atoi(string(expression[i]))
			} else {
				if operation == "*" {
					total *= atoi(string(expression[i]))
					operation = ""
				} else if operation == "+" {
					total += atoi(string(expression[i]))
					operation = ""
				}
			}
		}
	}
	return total
}

func readInput() []string {
	file, err := os.Open("Day18/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	var expressions []string
	for scan.Scan() {
		line := scan.Text()
		expressions = append(expressions, line)
	}
	return expressions
}

func isAtoi(input string) bool {
	_, err := strconv.Atoi(input)
	if err != nil {
		return false
	}
	return true
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}