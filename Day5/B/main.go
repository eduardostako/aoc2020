package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
)

const (
	Rows = 128
	Columns = 8
)

var seats []string

func main() {
	readInput()
	var seatIDs []int
	for _, seat := range seats {
		row := Rows
		column := Columns
		for i, pos := range seat{
			switch string(pos) {
			case "F":
				row -= Rows/int(math.Pow(2,float64(i+1)))
			case "L":
				column -= Columns/int(math.Pow(2,float64(i-6)))
			}
		}
		// Seat are zero indexed. This little hack below will fix it
		seatID := (row - 1)*8 + column - 1
		seatIDs = append(seatIDs, seatID)
	}
	sort.Ints(seatIDs)
	for i := 0; i < len(seatIDs) - 1; i++{
		if seatIDs[i] + 2 == seatIDs[i+1] {
			fmt.Println(seatIDs[i] + 1)
		}
	}
}

func readInput() []string {
	file, err := os.Open("Day5/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		seat := scan.Text()
		seats = append(seats, seat)
	}

	return seats
}