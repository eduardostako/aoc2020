package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

const (
	Preamble = 25
)

func main() {

	numbers := readInput()

	for i, number := range numbers[Preamble:] {
		preamble := numbers[i:Preamble + i]
		if !findNumber(preamble, number) {
			break
			fmt.Println(number)
		}
	}

}

func findNumber(preamble []int, number int) bool {
	for _, x := range preamble {
		for _, y := range preamble {
			if x != y && x+y == number {
				return true
			}
		}
	}
	return false
}

func readInput() []int {
	file, err := os.Open("Day9/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var numbers []int

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		numbers = append(numbers, atoi(line))

	}
	return numbers
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}