package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	Preamble = 25
)

func main() {
	defer timeTrack(time.Now(), "Day 9 B")
	numbers := readInput()

	for i, number := range numbers[Preamble:] {
		preamble := numbers[i:Preamble + i]
		if !findNumber(preamble, number) {
			fmt.Println(findContiguous(number, numbers))
			break
		}
	}
}

func findContiguous(numberToFind int, numbers []int) int {
	highestIndex, lowestIndex := findRange(numberToFind, numbers)
	min := numbers[lowestIndex]
	max := numbers[lowestIndex]
	for _, number := range numbers[lowestIndex:highestIndex] {
		if number < min {
			min = number
		} else if number > max {
			max = number
		}
	}
	return min + max
}

func findRange(numberToFind int, numbers []int) (int, int) {
	var total, highestIndex, lowestIndex int
	for i, number := range numbers {
		total += number
		if total == numberToFind {
			return highestIndex, lowestIndex
		} else {
			highestIndex = i
			for total >= numberToFind {
				total -= numbers[lowestIndex]
				lowestIndex++
				if total == numberToFind {
					return highestIndex, lowestIndex
				}
			}
		}
	}
	return highestIndex, lowestIndex
}

func findNumber(preamble []int, number int) bool {
	for _, x := range preamble {
		for _, y := range preamble {
			if x != y && x+y == number {
				return true
			}
		}
	}
	return false
}

func readInput() []int {
	file, err := os.Open("Day9/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var numbers []int

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		numbers = append(numbers, atoi(line))

	}
	return numbers
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func timeTrack(start time.Time, name string) {
	now := time.Now()
	elapsed := now.Sub(start)
	log.Printf("%s took %s", name, elapsed)
}