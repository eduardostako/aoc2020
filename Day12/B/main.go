package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

var directions = []string{"N", "E", "S", "W"}

type pos struct {
	x, y int
}

func (p pos) distance() int {
	return abs(p.y) + abs(p.x)
}

func (p *pos) sum(other pos)  {
	p.x += other.x
	p.y += other.y
}

func (p pos) scalar(value int) pos {
	return pos{p.x * value, p.y * value}
}

func main() {
	currentPos := pos{0, 0}
	waypointPos := pos{10, 1}
	instructions := readInput()
	for _, instruction := range instructions {
		dir, value := parseInstruction(instruction)
		currentPos, waypointPos = move(dir, currentPos, value, waypointPos)
	}

	fmt.Println(currentPos.distance())
}

func move(dir string, currentPos pos, value int, waypointPos pos) (pos, pos){
	switch dir {
	case "N":
		waypointPos.y += value
	case "S":
		waypointPos.y -= value
	case "E":
		waypointPos.x += value
	case "W":
		waypointPos.x -= value
	case "L":
		currentDirection := -value/90 + 4
		waypointPos = rotateWaypoint(currentDirection, waypointPos)
	case "R":
		currentDirection := value/90
		waypointPos = rotateWaypoint(currentDirection, waypointPos)
	case "F":
		currentPos.sum(waypointPos.scalar(value))
	}
	return currentPos, waypointPos
}

func rotateWaypoint(direction int, waypointPos pos) pos {
	var newWaypointPos pos
	if direction == 1 {
		newWaypointPos.x = waypointPos.y
		newWaypointPos.y = -waypointPos.x
	} else if direction == 2 {
		newWaypointPos.x = -waypointPos.x
		newWaypointPos.y = -waypointPos.y
	} else if direction == 3 {
		newWaypointPos.x = -waypointPos.y
		newWaypointPos.y = waypointPos.x
	}
	return newWaypointPos
}

func parseInstruction(instruction string) (string, int) {
	regex := regexp.MustCompile(`(N|S|E|W|L|R|F)([0-9]{1,3})`)
	res := regex.FindAllStringSubmatch(instruction, -1)

	return res[0][1], atoi(res[0][2])
}

func readInput() []string {
	file, err := os.Open("Day12/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)

	}
	return instructions
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}