package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

var directions = []string{"N", "E", "S", "W"}

type pos struct {
	x, y int
}

func (p pos) distance() int {
	return abs(p.y) + abs(p.x)
}

func main() {

	currentDirection := 1
	currentPos := pos{0, 0}
	instructions := readInput()
	for _, instruction := range instructions {
		dir, value := parseInstruction(instruction)
		currentDirection, currentPos = move(dir, currentPos, value, currentDirection)
	}

	fmt.Println(currentPos.distance())

}

func move(dir string, currentPos pos, value int, currentDirection int) (int, pos){
	switch dir {
	case "N":
		currentPos.y += value
	case "S":
		currentPos.y -= value
	case "E":
		currentPos.x += value
	case "W":
		currentPos.x -= value
	case "L":
		currentDirection = (currentDirection - value/90) % 4
		if currentDirection < 0 {
			currentDirection += 4
		}
	case "R":
		currentDirection = (currentDirection + value/90) % 4
	case "F":
		_, currentPos = move(directions[currentDirection], currentPos, value, currentDirection)
	}
	return currentDirection, currentPos
}

func parseInstruction(instruction string) (string, int) {
	regex := regexp.MustCompile(`(N|S|E|W|L|R|F)([0-9]{1,3})`)
	res := regex.FindAllStringSubmatch(instruction, -1)

	return res[0][1], atoi(res[0][2])
}

func readInput() []string {
	file, err := os.Open("Day12/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)

	}
	return instructions
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}