package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {

	var memory = make(map[int]int)
	var mask string
	lines := readInput()
	for _, line := range lines {
		if strings.Contains(line, "mask") {
			regex := regexp.MustCompile(`mask = ([0|1|X]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			mask = res[0][1]
		} else {
			regex := regexp.MustCompile(`mem\[([0-9]*)] = ([0-9]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			binary := atoi(res[0][2])
			memories := applyMask(res[0][1], mask)
			for _, memoryDir := range memories {
				memory[memoryDir] = binary
			}
		}
	}

	var total int
	for _, value := range memory {
		total += value
	}
	fmt.Println(total)
}

func convertToBinaryWithPadding(value int) string {
	s := strconv.FormatInt(int64(value), 2)
	return fmt.Sprintf("%036s", s)
}

func applyMask(memory string, mask string) []int {
	var memories = make(map[string]struct{})
	var memDir []int
	memories[convertToBinaryWithPadding(atoi(memory))] = struct{}{}

	for i, bit := range mask {
		if string(bit) == "X" {
			for address := range memories {
				runeAddress := []rune(address)
				runeAddress[i] = 48
				memories[string(runeAddress)] = struct{}{}
				runeAddress[i] = 49
				memories[string(runeAddress)] = struct{}{}
			}
		} else if string(bit) == "1" {
			var tempMemories = make(map[string]struct{})
			for address := range memories {
				runeAddress := []rune(address)
				runeAddress[i] = 49
				tempMemories[string(runeAddress)] = struct{}{}
			}
			memories = tempMemories
		}
	}
	for mem := range memories {
		if value, err := strconv.ParseInt(mem, 2, 64); err == nil {
			memDir = append(memDir, int(value))
		} else {
			log.Fatal(err)
		}
	}
	return memDir
}

func readInput() []string {
	file, err := os.Open("Day14/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)
	}
	return instructions
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
