package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {

	var memory = make(map[int]int)
	var mask string
	lines := readInput()
	for _, line := range lines {
		if strings.Contains(line, "mask") {
			regex := regexp.MustCompile(`mask = ([0|1|X]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			mask = res[0][1]
		} else {
			regex := regexp.MustCompile(`mem\[([0-9]*)] = ([0-9]*)`)
			res := regex.FindAllStringSubmatch(line, -1)
			binary := []rune(convertToBinaryWithPadding(atoi(res[0][2])))
			for i, bit := range mask {
				if string(bit) != "X" {
					binary[i] = bit
				}
			}
			if value, err := strconv.ParseInt(string(binary), 2, 64); err != nil {
				log.Fatal(err)
			} else {
				memory[atoi(res[0][1])] = int(value)
			}
		}
	}

	var total int
	for _, value := range memory {
		total += value
	}
	fmt.Println(total)
}

func convertToBinaryWithPadding(value int) string {
	s := strconv.FormatInt(int64(value), 2)
	return fmt.Sprintf("%036s", s)
}

func readInput() []string {
	file, err := os.Open("Day14/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)
	}
	return instructions
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
