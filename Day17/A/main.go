package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type coor struct {
	x,y,z int
}

var vectors = []coor{
	{-1,-1,-1}, {-1,-1,0}, {-1,-1,1},
	{-1,0,-1},  {-1,0,0},  {-1,0,1},
	{-1,1,-1},  {-1,1,0},  {-1,1,1},
	{0,-1,-1},  {0,-1,0},  {0,-1,1},
	{0,0,-1},   {0,0,1},
	{0,1,-1},   {0,1,0},   {0,1,1},
	{1,-1,-1},  {1,-1,0},  {1,-1,1},
	{1,0,-1},   {1,0,0},   {1,0,1},
	{1,1,-1},   {1,1,0},   {1,1,1}}

func (c coor) sumCoors(other coor) coor {
	return coor{c.x + other.x, c.y + other.y, c.z + other.z}
}

func main() {
	cubes := readInput()
	for i := 0; i < 6; i++ {
		for origin := range cubes {
			for _, vector := range vectors {
				tempCoor := origin.sumCoors(vector)
				if _, ok := cubes[tempCoor]; !ok {
					cubes[tempCoor] = "."
				}
			}
		}
		cubes = generateIteration(cubes)
		var totalActive int
		for _, state := range cubes {
			if state == "#" {
				totalActive++
			}
		}
	}

	var totalActive int
	for _, state := range cubes {
		if state == "#" {
			totalActive++
		}
	}
	fmt.Println(totalActive)

}

func generateIteration(cubes map[coor]string) map[coor]string {
	var newCubes = make(map[coor]string)
	for k,v := range cubes {
		newCubes[k] = v
	}
	for origin, state := range cubes {
		activeCubes := getActiveCubes(origin, cubes)
		if state == "#" {
			if activeCubes != 2 && activeCubes != 3 {
				newCubes[origin] = "."
			}
		} else {
			if activeCubes == 3 {
				newCubes[origin] = "#"
			}
		}
	}
	return newCubes
}

func getActiveCubes(origin coor, cubes map[coor]string) int {
	var totalActive int
	for _, vector := range vectors {
		tempCoor := origin.sumCoors(vector)
		if cubes[tempCoor] == "#" {
			totalActive++
		}
	}
	return totalActive
}

func readInput() map[coor]string {
	file, err := os.Open("Day17/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	var coors = make(map[coor]string)
	var y int
	for scan.Scan() {
		line := scan.Text()
		for x, cube := range line {
			dir := coor{x, y, 0}
			coors[dir] = string(cube)
		}
		y++
	}
	return coors
}