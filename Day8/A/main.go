package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	instructions := readInput()

		accumulator := 0
		passedBy := make(map[int]bool)
		for i := 0; i <= len(instructions); i++ {
			instruction, offset := parseInstruction(instructions[i])
			if _, ok := passedBy[i]; ok {
				break
			} else {
				passedBy[i] = true
			}
			switch instruction {
			case "acc":
				accumulator += offset
			case "jmp":
				i += offset - 1
			case "nop":
			}
		}


    fmt.Println(accumulator)
}

func parseInstruction(instruction string) (string, int) {
	regex := regexp.MustCompile(`(nop|acc|jmp) (.[0-9]*)`)
	res := regex.FindAllStringSubmatch(instruction, -1)

	return res[0][1], atoi(res[0][2])
}

func readInput() []string {
	file, err := os.Open("Day8/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)

	}
	return instructions
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}