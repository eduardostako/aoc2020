package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	instructions := readInput()
	for j := 0; j < len(instructions); j++ {
		instruction, _ := parseInstruction(instructions[j])
		if instruction != "acc" {
			tempInstructions := make([]string, len(instructions))
			copy(tempInstructions, instructions)
			if instruction == "nop" {
				tempInstructions[j] = strings.Replace(tempInstructions[j], "nop", "jmp", 1)
			} else {
				tempInstructions[j] = strings.Replace(tempInstructions[j], "jmp", "nop", 1)

			}
			finish := true
			passedBy := make(map[int]bool)
			accumulator := 0
			for i := 0; i < len(tempInstructions); i++ {
				instruction, offset := parseInstruction(tempInstructions[i])
				if _, ok := passedBy[i]; ok {
					finish = false
					break
				} else {
					passedBy[i] = true
				}
				switch instruction {
				case "acc":
					accumulator += offset
				case "jmp":
					i += offset - 1
				case "nop":
				}
			}
			if finish {
				fmt.Println(accumulator)
				break
			}
		}
	}
}

func parseInstruction(instruction string) (string, int) {
	regex := regexp.MustCompile(`(nop|acc|jmp) (.[0-9]*)`)
	res := regex.FindAllStringSubmatch(instruction, -1)

	return res[0][1], atoi(res[0][2])
}

func readInput() []string {
	file, err := os.Open("Day8/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var instructions []string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		instructions = append(instructions, line)

	}
	return instructions
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}