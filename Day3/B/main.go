package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var terrain [][]bool

func main() {
	readInput()
	mult := 1
	slopes := [][]int{{1,1},{3,1},{5,1},{7,1},{1,2}}
	for _, slope := range slopes {
		trees := calculateTrees(slope[0],slope[1])
		mult *= trees
	}
	fmt.Println(mult)

}

func calculateTrees(x int, y int) int{
	var trees int
	step := 1
	for i := y; i < len(terrain); i = i + y {
		if terrain[i][x*step%len(terrain[i])]{
			trees++
		}
		step++
	}
	return trees
}

func readInput() {
	file, err := os.Open("Day3/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		var levelMap []bool
		level := scan.Text()
		for _, square := range level {
			if string(square) == "#" {
				levelMap = append(levelMap, true)
			}else{
				levelMap = append(levelMap, false)
			}
		}
		terrain = append(terrain, levelMap)
	}
}