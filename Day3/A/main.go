package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	terrain := readInput()
	var trees int
	for i := 1; i < len(terrain); i++ {
		if terrain[i][(1*i)%len(terrain[i])]{
			trees++
		}
	}
	fmt.Println(trees)


}

func readInput() [][]bool {
	file, err := os.Open("Day3/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var terrain [][]bool

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		var levelMap []bool
		level := scan.Text()
		for _, square := range level {
			if string(square) == "#" {
				levelMap = append(levelMap, true)
			}else{
				levelMap = append(levelMap, false)
			}
		}
		terrain = append(terrain, levelMap)
	}

	return terrain
}