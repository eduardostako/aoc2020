package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

var passports []string
var fields = []string {
	"byr",
	"iyr",
	"eyr",
	"hgt",
	"hcl",
	"ecl",
	"pid",
}

func main() {

	var valid int
	readInput()
	for _, passport := range passports {
		containField := 0
		for _, field := range fields {
			if strings.Contains(passport, field) {
				containField++
			}
		}
		if containField == 7 {
			valid++
		}
	}
	fmt.Println(valid)
}

func readInput() []string {
	file, err := os.Open("Day4/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var passport string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if len(line) != 0 {
			passport += line + " "
		} else {
			passports = append(passports, passport)
			passport = ""
		}
	}
	passports = append(passports, passport)

	return passports
}