package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var passports []string
var fields = []string {
	"byr",
	"iyr",
	"eyr",
	"hgt",
	"hcl",
	"ecl",
	"pid",
}

func main() {

	var valid int
	readInput()
	for _, passport := range passports {
		validField := 0
		for _, field := range fields {
			if strings.Contains(passport, field) && checkFieldData(passport, field){
				validField++
			}
		}
		if validField == 7 {
			valid++
		}
	}
	fmt.Println(valid)
}

func checkFieldData(passport string, field string) bool {
	switch field {
	case "byr":
		regex := regexp.MustCompile(`byr:([0-9]{4})`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil && atoi(res[0][1]) >= 1920 && atoi(res[0][1]) <= 2002
	case "iyr":
		regex := regexp.MustCompile(`iyr:([0-9]{4})`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil && atoi(res[0][1]) >= 2010 && atoi(res[0][1]) <= 2020
	case "eyr":
		regex := regexp.MustCompile(`eyr:([0-9]{4})`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil && atoi(res[0][1]) >= 2020 && atoi(res[0][1]) <= 2030
	case "hgt":
		regex := regexp.MustCompile(`hgt:([0-9]*)(cm|in)`)
		res := regex.FindAllStringSubmatch(passport, -1)
		if res != nil {
			if res[0][2] == "cm" {
				return atoi(res[0][1]) >= 150 && atoi(res[0][1]) <= 193
			} else if res[0][2] == "in" {
				return atoi(res[0][1]) >= 59 && atoi(res[0][1]) <= 76
			}
		}
	case "hcl":
		regex := regexp.MustCompile(`hcl:#(([0-9]|[a-f]){6})`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil
	case "ecl":
		regex := regexp.MustCompile(`ecl:(amb|blu|brn|gry|grn|hzl|oth)`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil
	case "pid":
		regex := regexp.MustCompile(`pid:([0-9]{9})( |$)`)
		res := regex.FindAllStringSubmatch(passport, -1)
		return res != nil
	}

	return false
}
func readInput() []string {
	file, err := os.Open("Day4/input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var passport string

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if len(line) != 0 {
			passport += line + " "
		} else {
			passports = append(passports, passport)
			passport = ""
		}
	}
	passports = append(passports, passport)

	return passports
}

func atoi (input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}